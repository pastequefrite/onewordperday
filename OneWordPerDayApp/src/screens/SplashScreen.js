import React from 'react';
import {SafeAreaView, ActivityIndicator, Text, StyleSheet, Image, StatusBar} from 'react-native';

class SplashScreen extends React.Component {
    render() {
        return (
            <SafeAreaView style={style.container}>
                <StatusBar barStyle="light-content" />
                <Image source={require('../../assets/book-open-outline-filled.png')}/>
                <Text style={style.title}>Un jour, un mot !</Text>
                <Text style={style.subTitle}>{this.props.loadInfo}</Text>
                <ActivityIndicator style={style.loader} size={"large"} animating={true} color={"white"} />
            </SafeAreaView>
        );
    }
}

const style = StyleSheet.create({
    container: {
        backgroundColor : '#34495e',
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    title: {
        color: 'white',
        fontSize: 30,
        fontWeight: 'bold',
    },
    subTitle: {
        color: 'white',
        fontSize: 17,
    },
    loader: {
        padding: 20,
    }
});


export default SplashScreen;
