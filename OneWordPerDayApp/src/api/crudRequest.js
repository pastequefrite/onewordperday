export const executeGetRequest = async (apiEndPoint) => {
    try {
        const response = await fetch(apiEndPoint, {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
        });

        let responseJson = response.json();
        return responseJson;
    } catch (error) {
        console.warn(apiEndPoint + '/' + error);
    }
};
