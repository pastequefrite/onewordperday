import config from './apiConfig';
import {executeGetRequest} from './crudRequest';


export const getRandomWord = async () => {
    return await executeGetRequest(config.url + '/mots/motauhasard?avecdef=true&verbeconjugue=false&api_key='+config.secret);
};

export const getDefinitionWord = async (word) => {
    return await executeGetRequest(config.url + '/mot/'+word+'/definitions?limite=200&api_key='+config.secret);
};

export const getSynonymeWord = async (word) => {
    return await executeGetRequest(config.url + '/mot/'+word+'/synonymes?limite=5&api_key='+config.secret);
};

export const getAntonymesWord = async (word) => {
    return await executeGetRequest(config.url + '/mot/'+word+'/antonymes?limite=5&api_key='+config.secret);
};


export const getQuotesWord = async (word) => {
    return await executeGetRequest(config.url + '/mot/'+word+'/citations?limite=5&api_key='+config.secret);
};

export const getExpressionsWord = async (word) => {
    return await executeGetRequest(config.url + '/mot/'+word+'/expressions?limite=5&api_key='+config.secret);
};

export const getScrabbleScoreWord = async (word) => {
    return await executeGetRequest(config.url + '/mot/'+word+'/scorescrabble?api_key='+config.secret);
};
