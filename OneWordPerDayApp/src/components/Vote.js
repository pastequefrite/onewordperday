import React from 'react';
import {View, Text, StyleSheet} from 'react-native';
import EFButton from './EFButton';


class Vote extends React.Component {

    onButtonPress = (value) => {
        alert(value);
        if (value) {
            //Je connaissais
        } else {
            //Je ne connaissais pas
        }
    };


    render() {
        return (
            <View style={style.container}>
                <Text style={style.title}>Connaissiez vous ce mot ?</Text>
                <View style={style.buttons}>
                    <EFButton title={'Oui'} buttonIcon={'check'} buttonStyle={style.btnYes} onPress={() => this.onButtonPress(true)}/>
                    <EFButton title={'Non'} buttonIcon={'times'} buttonStyle={style.btnNo} onPress={() => this.onButtonPress(false)}/>
                </View>
            </View>
        );
    }
}

const style = StyleSheet.create({
    container: {
        // flex: 1,
        justifyContent: 'center',
    },
    title: {
        color: 'white',
        fontSize: 20,
        alignSelf: 'center',
    },
    buttons: {
        padding: 10,
        flexDirection: 'row',
        justifyContent: 'space-around',
    },
    btnYes: {
        backgroundColor: 'green',
        paddingHorizontal: 25,
        paddingVertical: 10,
    },
    btnNo: {
        backgroundColor: 'red',
        paddingHorizontal: 25,
        paddingVertical: 10,
    },
});

export default Vote;
