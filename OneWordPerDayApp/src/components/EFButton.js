import React from 'react';
import {View, StyleSheet} from 'react-native';

import Icon from 'react-native-vector-icons/FontAwesome5';
import {Button} from 'react-native-elements';

class EFButton extends React.Component {


    render() {
        return (
            <View style={styles.buttonContainer}>
                <Button buttonStyle={this.props.buttonStyle}
                        iconRight
                        icon={
                            <Icon
                                name={this.props.buttonIcon}
                                size={15}
                                color="white"
                                style={{paddingLeft: 10}}
                            />}
                        title={this.props.title}
                        onPress={this.props.onPress}
                />
            </View>
        )
    }
}

const styles = StyleSheet.create({
    buttonContainer: {
        justifyContent: 'center',
        alignItems: 'center',
    },
});

export default EFButton;
