import React from 'react';
import {View, StyleSheet, Text} from 'react-native';


class KeyValueWord extends React.Component{
    render() {
        return(
            <View style={style.content}>
                <Text style={style.title}>{this.props.title}</Text>
                <View style={style.contentValue}>
                <Text style={style.value}>{this.props.value ? this.props.value : "Aucune donnée trouvée"}</Text>
                </View>
            </View>
        );
    }
}


const style = StyleSheet.create({
    content: {
        // flex: 1,
        margin: 10,
        alignItems: 'stretch',
        justifyContent: 'center',
    },
    title: {
        fontSize: 20,
        color: 'white',
        textDecorationLine: 'underline',
    },
    contentValue: {
        flexDirection: 'row',
        flexWrap: 'wrap',
        paddingHorizontal: 15,
    },
    value: {
        color: 'white',
        fontSize: 18,
    }
});


export default KeyValueWord;
