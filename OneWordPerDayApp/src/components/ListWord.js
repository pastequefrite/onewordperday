import React from 'react';
import {View, StyleSheet, Text} from 'react-native';


class ListWord extends React.Component {
    constructor(props) {
        super(props);

        this.wordsLength = this.props.words.length
    }


    render() {
        return (
            <View style={style.content}>
                <Text style={style.header}>{this.props.title}</Text>
                {
                    this.wordsLength > 0 ?
                        <View style={style.list}>
                            {
                                this.props.words.map((item, i) => {
                                    if (this.wordsLength === i + 1) {// le dernier
                                        return <Text style={style.item} key={item.dicolinkUrl}>{item.mot}.</Text>;
                                    }
                                    return <Text style={style.item} key={item.dicolinkUrl}>{item.mot} - </Text>;
                                })
                            }
                        </View>
                        :
                        <View style={style.list}><Text style={style.item}>Aucune donnée trouvée</Text></View>
                }
            </View>
        );
    }
}

const style = StyleSheet.create({
    content: {
        // flex: 1,
        margin: 10,
        alignItems: 'stretch',
        justifyContent: 'center',
    },
    header: {
        fontSize: 20,
        color: 'white',
        textDecorationLine: 'underline',
    },
    item: {
        fontSize: 18,
        color: 'white',
    },
    list: {
        flexDirection: 'row',
        flexWrap: 'wrap',
        paddingHorizontal: 15,
    }
});

export default ListWord;
