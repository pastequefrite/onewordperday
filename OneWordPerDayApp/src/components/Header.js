import React from 'react';
import {View, StyleSheet, Image, Text} from 'react-native';


class Header extends React.Component{
    render() {
        return(
            <View style={style.content}>
                <Image style={style.image} source={require('../../assets/book-open-outline-filled.png')}/>
                <Text style={style.title}>Un jour, un mot !</Text>
            </View>
        )
    }
}


const style = StyleSheet.create({
    image: {
        resizeMode: 'contain',
        height: 50,
    },
    content: {
        alignItems: 'center',
        margin: 10,
    },
    title: {
        color: 'white',
        fontSize: 25,
        fontWeight: 'bold',
    },
});

export default Header;
