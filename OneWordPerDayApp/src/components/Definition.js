import React from 'react';
import {View, StyleSheet, Text} from 'react-native';


class Definition extends React.Component{
    render() {
        return(
            <View style={style.content}>
                <Text style={style.defHeader}>
                    <Text style={style.word}>{this.props.word.mot} </Text>
                    <Text>{this.props.word.nature ? "(" + this.props.word.nature + ")" : ""}</Text>
                    <Text> : </Text>
                </Text>
                <View style={style.testContainer}>
                    <Text style={style.defContent}>{this.props.word.definition}</Text>
                </View>
            </View>
        );
    }
}


const style = StyleSheet.create({
    content: {
        // flex: 1,
        margin: 10,
        alignItems: 'stretch',
        justifyContent: 'center',
    },
    defHeader: {
        fontSize: 22,
        color: 'white',
    },
    testContainer: {
        flexDirection: 'row',
        flexWrap : 'wrap',
    },
    defContent: {
        fontSize: 17,
        color: 'white',
        fontStyle: 'italic',
        paddingHorizontal: 15,
    },
    word : {
        fontWeight: 'bold',
    },
});


export default Definition;
