import React from 'react';
import {createAppContainer, createBottomTabNavigator, createStackNavigator} from 'react-navigation';
import Icon from 'react-native-vector-icons/FontAwesome5';

import MainScreen from '../screens/MainScreen';
import FavoriteScreen from '../screens/FavoriteScreen';
import SettingsScreen from '../screens/SettingsScreen';


// Favorite StackNavigator
const FavoriteStackNavigator = createStackNavigator({
    Favorite: {
        screen: FavoriteScreen,
        navigationOptions: {
            title: 'Favoris',
        },
    },
});

// Settings StackNavigator
const SettingsStackNavigator = createStackNavigator({
    Settings: {
        screen: SettingsScreen,
        navigationOptions: {
            title: 'Réglages',
        },
    },
});

// App Tab Navigator
const AppNavigator = createBottomTabNavigator(
    {
        Main: {
            screen: MainScreen,
            navigationOptions: {
                title: 'Mot du jour',
                tabBarIcon: ({tintColor}) => <Icon type="font-awesome" name="home" size={25} color={tintColor}/>,
            },
        },
        Favorite: {
            screen: FavoriteStackNavigator,
            navigationOptions: {
                title: 'Favoris',
                tabBarIcon: ({tintColor}) => <Icon type="font-awesome" name="star" size={25} color={tintColor}/>,
            },
        },
        Settings: {
            screen: SettingsStackNavigator,
            navigationOptions: {
                title: 'Favoris',
                tabBarIcon: ({tintColor}) => <Icon type="font-awesome" name="cogs" size={25} color={tintColor}/>,
            },
        },
    },
    {
        tabBarOptions: {
            activeTintColor: '#34495e',
            showLabel: false,
            showIcon: true,
        },
    },
);


export default createAppContainer(AppNavigator);
